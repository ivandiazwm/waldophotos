import React from 'react';
import PropTypes from 'prop-types';
import { find, includes} from 'lodash';
import { Button, ListGroup, Dropdown, Spinner } from 'react-bootstrap';

import './pizza-selector.css';
class PizzaSelector extends React.Component {
    static propTypes = {
        pizzaSizes: PropTypes.arrayOf(PropTypes.shape({
            name: PropTypes.string,
            maxToppings: PropTypes.number,
            basePrice: PropTypes.number,
        })),
        pizzaSizesLoaded: PropTypes.bool,
        toppings: PropTypes.arrayOf(PropTypes.shape({
            name: PropTypes.string,
            price: PropTypes.number
        })),
        toppingsLoaded: PropTypes.bool,
        onPizzaSizeSelected: PropTypes.func,
        onSubmit: PropTypes.func,
        onChange: PropTypes.func,
        value: PropTypes.shape({
            size: PropTypes.string,
            toppings: PropTypes.arrayOf(PropTypes.string)
        }),
    };

    static defaultProps = {
        pizzaSizesLoaded: false,
        toppingsLoaded: false,
        pizzaSizes: [],
        toppings: [],
        value: {
            size: '',
            toppings: [],
            price: 0
        },
    };

    render = () => {
        const {pizzaSizesLoaded, toppingsLoaded} = this.props;

        return (
            <div className="pizza-selector">
                <div className="pizza-selector__header">
                    <h3>Select Pizza</h3>
                </div>
                {pizzaSizesLoaded ? this.renderPizzaSizeDropdown() : <Spinner animation="border" />}
                <div className="pizza-selector__toppings">
                    {toppingsLoaded ? this.renderToppings() : <Spinner animation="border" />}
                </div>
                <div className="pizza-selector__add-pizza">
                    <div>Price: <span className="pizza-selector__price">${Number(this.props.value.price).toFixed(2)}</span></div>
                    {this.props.value.size ? <Button variant="primary" onClick={this.onAddPizza}>Add Pizza</Button> : null}
                </div>
            </div>
        );
    };

    renderPizzaSizeDropdown = () => {
        const {value, pizzaSizes} = this.props;

        return (
            <Dropdown onSelect={this.onSizeSelected}>
                <Dropdown.Toggle variant="success" id="dropdown-basic">
                    Size
                </Dropdown.Toggle>

                <Dropdown.Menu>
                    {pizzaSizes.map(
                        (options, index) => (
                            <Dropdown.Item
                                key={index}
                                eventKey={index}
                                active={options.name === this.getPizzaSizeOptions(value.size).name}>
                                {options.name} <span className="pizza-selector__price">(${options.basePrice})</span>
                            </Dropdown.Item>
                        )
                    )}
                </Dropdown.Menu>
            </Dropdown>
        );
    };

    renderToppings = () => {
        const {value, toppings} = this.props;
        const {maxToppings} = this.getPizzaSizeOptions(value.size);

        if(!toppings.length) {
            return null;
        }

        return (
            <div>
                <div>Toppings</div>
                <ListGroup>
                    {toppings.map(
                        (topping, index) => {
                            const checked = includes(value.toppings, topping.name);
                            return (
                                <ListGroup.Item key={index}>
                                    <label>
                                        <input
                                            type="checkbox"
                                            checked={checked}
                                            onChange={this.onToppingToggled.bind(this, index)}
                                            disabled={maxToppings !== null && value.toppings.length >= maxToppings && !checked}
                                        />
                                        {topping.name} <span className="pizza-selector__price">(+${topping.price})</span>
                                    </label>
                                </ListGroup.Item>
                            )
                        }
                    )}
                </ListGroup>
            </div>
        );
    };

    getPizzaSizeOptions = size => {
        return find(this.props.pizzaSizes, {name: size}) || {};
    };

    onSizeSelected = eventKey => {
        this.props.onPizzaSizeSelected(this.props.pizzaSizes[eventKey].name);
    };

    onToppingToggled = index => {
        const { toppings, value } = this.props;
        const toppingName = toppings[index].name;
        const toppingPrice = toppings[index].price;

        this.props.onChange({
            ...value,
            toppings: includes(value.toppings, toppingName) ? value.toppings.filter(name => name !== toppingName) : [...value.toppings, toppingName],
            price: value.price + (includes(value.toppings, toppingName) ? -toppingPrice : toppingPrice)
        });
    };

    onAddPizza = () => {
        this.props.onSubmit(this.props.value);
    }
}

export default PizzaSelector;