import React from 'react';
import PropTypes from 'prop-types';
import { ListGroup } from 'react-bootstrap';
import { FaTimes } from 'react-icons/fa';

import './cart.css';
class Cart extends React.Component {
    static propTypes = {
        items: PropTypes.arrayOf(PropTypes.shape({
            size: PropTypes.string,
            price: PropTypes.number,
            toppings: PropTypes.arrayOf(PropTypes.string)
        })),
        total: PropTypes.number,
        onDeleteItem: PropTypes.func,
    };

    static defaultProps = {
        items: [],
        total: 0,
    };

    render = () => {
        const {items, total} = this.props;

        return (
            <div className="cart">
                <h3>Cart</h3>
                <div>
                    <ListGroup>
                        {items.map((item, index) => (
                            <ListGroup.Item className="cart__item" key={index}>
                                <div>
                                    <div className="cart__item-name">
                                        {item.size} pizza <span className="cart__price">${Number(item.price).toFixed(2)}</span>
                                    </div>
                                    <small>{item.toppings.join(', ')}</small>
                                </div>
                                <FaTimes className="cart__delete-button" onClick={this.onDeleteClick.bind(this, index)} />
                            </ListGroup.Item>
                        ))}
                    </ListGroup>
                </div>
                {!items.length ? <div className="cart__empty-message">Empty cart</div> : null}
                <div className="cart__total">
                    Total: <span className="cart__price">${Number(total).toFixed(2)}</span>
                </div>
            </div>
        );
    };

    onDeleteClick = (index, event) => {
        event.preventDefault();
        this.props.onDeleteItem(index);
    };
}

export default Cart;