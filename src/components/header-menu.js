import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';

import './header-menu.css';
class HeaderMenu extends React.Component {
    static propTypes = {
        total: PropTypes.number,
        onAddPizzaClick: PropTypes.func,
        onTotalClick: PropTypes.func,
    };

    render() {
        return (
            <div className="header-menu">
                <Button variant="info" onClick={this.props.onAddPizzaClick}>Add Pizza</Button>
                <Button variant="success" onClick={this.props.onTotalClick}>Total: ${Number(this.props.total).toFixed(2)}</Button>
            </div>
        )
    }
}

export default HeaderMenu;