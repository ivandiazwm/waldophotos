import React from 'react';

import './App.css';
import HeaderMenu from './components/header-menu';
import PizzaForm from './pizza-form';
import Cart from './components/cart';


class App extends React.Component {
    state = {
        showCart: false,
        cart: {
            items: [],
            total: 0,
        }
    };


    render = () => {
        const { showCart, cart } = this.state;

        return (
            <div className="app">
                <HeaderMenu total={cart.total}
                            onAddPizzaClick={() => this.setState({showCart: false})}
                            onTotalClick={() => this.setState({showCart: true})} />
                {showCart ? <Cart {...cart} onDeleteItem={this.onDeletePizza} /> : this.renderPizzaForm()}
            </div>
        );
    };

    renderPizzaForm = () => {
        return (
            <PizzaForm onAddPizza={this.onAddPizza} />
        );
    };

    onAddPizza = value => {
        this.setState({
            showCart: true,
            cart: {
                items: [...this.state.cart.items, value],
                total: this.state.cart.total + value.price
            }
        });
    };

    onDeletePizza = deleteIndex => {
        this.setState({
            showCart: true,
            cart: {
                items: this.state.cart.items.filter((item, index) => index !== deleteIndex),
                total: this.state.cart.total - this.state.cart.items[deleteIndex].price
            }
        });
    }
}

export default App;
