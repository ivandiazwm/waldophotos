import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { find } from 'lodash';

import PizzaSelector from './components/pizza-selector';

const PIZZA_GRAPHQL_URL = 'https://core-graphql.dev.waldo.photos/pizza';

class PizzaForm extends React.Component {
    static propTypes = {
        onAddPizza: PropTypes.func
    };

    state = {
        error: null,
        pizzaSizesLoaded: false,
        toppingsLoaded: true,
        pizzaSizes: [],
        toppings: [],
        value: {
            size: '',
            toppings: [],
            price: 0
        },
    };

    componentDidMount = () => this.requestPizzaSizes();

    render = () => {
        if(this.state.error) {
            return <div>{this.state.error}</div>;
        }

        return (
            <PizzaSelector
                {...this.state}
                onPizzaSizeSelected={pizza => this.setState({value: {size: pizza, price: 0, toppings: []}}, () => this.requestToppingsOf(pizza))}
                onChange={value => this.setState({value})}
                onSubmit={this.props.onAddPizza}
            />
        );
    };

    requestPizzaSizes = () => {
        this.setState({pizzaSizes: [], pizzaSizesLoaded: false});

        return this.requestData(`
            query {
                pizzaSizes {
                    name,
                    maxToppings,
                    basePrice
                }
            }
        `).then(result => this.setState({
            pizzaSizesLoaded: true,
            pizzaSizes: result.data.pizzaSizes,
        })).catch(() => this.setState({error: 'Error requesting pizza sizes'}));
    };

    requestToppingsOf = pizza => {
        const SIZES = {
            'small': 'SMALL',
            'medium': 'MEDIUM',
            'large': 'LARGE'
        };

        this.setState({toppings: [], toppingsLoaded: false});

        return this.requestData(`
            query {
                pizzaSizeByName(name: ${SIZES[pizza]}) {
                    toppings {
                        topping {
                            name,
                            price,
                        },
                        defaultSelected
                    }
                }
            }
        `)
            .then(result => this.resetToppings(result.data.pizzaSizeByName.toppings))
            .catch(() => this.setState({error: 'Error requesting toppings'}));
    };

    resetToppings = toppings => {
        const { value, pizzaSizes } = this.state;
        const defaultToppings = toppings.filter(item => item.defaultSelected);
        const pizzaSizePrice = (find(pizzaSizes, {name: value.size}) || {}).basePrice || 0;

        this.setState({
            toppingsLoaded: true,
            toppings: toppings.map(item => item.topping),
            value: {
                ...value,
                toppings: defaultToppings.map(item => item.topping.name),
                price: defaultToppings.reduce((sum, item) => sum + item.topping.price, pizzaSizePrice)
            }
        });
    };

    requestData = query => {
        this.setState({error: null});

        return axios({
            url: PIZZA_GRAPHQL_URL,
            method: 'POST',
            data: { query }
        }).then(result => result.data);
    }

}

export default PizzaForm;