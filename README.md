# waldophotos-challenge
`Done by Ivan Diaz`

This is a minimal React application for interviewing at `Waldo Photos` for a `Senior Frontend Engineer` position.

The app features a pizza cart using a provided GraphQL server.


### Running the project

* npm install
* npm start


## Images

![image1](https://i.ibb.co/PryhkNr/pizza1.png)
![image2](https://i.ibb.co/BrrGGmG/pizza2.png)
![image3](https://i.ibb.co/S63qsTH/pizza3.png)